import Logo from "@/assets/Logo";
import styles from "../page.module.css";
import { FORM_LINK } from "@/constants";

export default function Header({ctaClick}: {ctaClick: React.Dispatch<React.SetStateAction<boolean>>}) {
    return (
        <div className={styles.header}>
            <div className={styles.logo}>
                <div className={styles.logoImage}>
                    <Logo color="#ffffff"/>
                </div>
                <h1 className={styles.logoName}>elev8r</h1>
            </div>
            <button onClick={() => {window.open(FORM_LINK, '_blank')}} className={styles.headerButton}>Know more</button>
        </div>
    );
}