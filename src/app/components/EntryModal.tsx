import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { Input, TextField } from '@mui/material';

const style = {
    display: 'flex',
    gap: '15px',
    flexDirection: 'column',
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

export default function EntryModal({show, setShow}: {show: boolean, setShow: React.Dispatch<React.SetStateAction<boolean>>}) {
    const handleClose = () => {
        setShow(false);
        setShowError(false);
    }
    const [name, setName] = React.useState('');
    const [phoneNumber, setPhoneNumber] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [college, setCollege] = React.useState('');

    const [showError, setShowError] = React.useState(false);

    const setPhoneInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        e.target.value = e.target.value.replace(/[^0-9]/g, '');
        setPhoneNumber(e.target.value);
    }

    const handleSubmit = async () => {
        const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        const phoneNumberPattern = /^\d{10}$/;
        if (!emailPattern.test(email) || !phoneNumberPattern.test(phoneNumber)) {
            setShowError(true);
            return;
        }
        const data = new FormData();
        data.append('fullName', name);
        data.append('email', email);
        data.append('phoneNumber', phoneNumber);
        data.append('college', college);
        const Sheet_Url="https://script.google.com/macros/s/AKfycbw11cvl6igZdPCIr2Xjjs67lX_ZPIoxA5ZQo_6VH8V19ZpaL1TUVC5mZxmByLRRchfV/exec"
        try {
        await fetch(Sheet_Url, {
            method: 'POST',
            body: data,
        });

        setName('');
        setPhoneInput({ target: { value: '' } } as React.ChangeEvent<HTMLInputElement>);
        setEmail('');
        setCollege('');

        } catch (error) {
        console.log(error);
        }
        setShowError(false);
        handleClose();
    }

  return (
    <Modal
        open={show}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
    >
        <Box sx={style}>
            <Typography id="modal-modal-title" variant="h6" component="h2" style={{color: 'black', fontWeight: 'bolder'}}>
                Get ready to Elev8 your resume!
            </Typography>
            {showError && <Typography id="modal-modal-title" style={{color: 'red', fontWeight: '100px', fontSize: '14px'}}>
                * Invalid email and(or) phone number (should be 10 digits). </Typography>}
            
            <TextField 
                style={{width:'100%'}} 
                id="emailId" 
                label="Email ID*" 
                variant="outlined"
                onChange={(e) => setEmail(e.target.value)} />
            <TextField 
                style={{width:'100%'}} 
                id="phoneNumber" 
                label="Phone Number*" 
                variant="outlined"
                onChange={setPhoneInput} />
            <TextField 
                style={{width:'100%'}} 
                id="name" 
                label="Name" 
                variant="outlined"
                onChange={(e) => setName(e.target.value)} />
            <TextField 
                style={{width:'100%'}} 
                id="college" 
                label="College/Company name" 
                variant="outlined"
                onChange={(e) => setCollege(e.target.value)} />

            <Box style={{
                display: 'flex',
                flexDirection: 'row',
                gap: '8px',
                marginTop: '15px'
            }}>
                <Button 
                    style={{width:'100%', background: 'black'}} 
                    variant="contained"
                    onClick={handleSubmit}
                    color="primary">
                        Submit
                </Button>
                <Button 
                    style={{width:'100%', background: 'gray'}} 
                    variant="contained" 
                    onClick={handleClose}
                    color="secondary">
                        Cancel
                </Button>
            </Box>
        </Box>
    </Modal>
  );
}