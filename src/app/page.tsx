"use client";
import styles from "./page.module.css";
import Header from "./components/Header";
import Content from "./components/Content";
import Footer from "./components/Footer";
import { useState } from "react";
import EntryModal from "./components/EntryModal";

export default function Home() {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <main className={styles.main}>
      <div>
        <Header ctaClick={setIsOpen}/>
        <Content ctaClick={setIsOpen}/>
        <Footer/>
        {/* <EntryModal show={isOpen} setShow={setIsOpen}/> */}
      </div>
    </main>
  );
}
